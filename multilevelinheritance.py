class first:
    c1='first'
    def get_data(self,name,course):
        self.name=name
        self.course=course
        print(name,course)

class second(first):
    colors=['red','green']
    def clrs(self):
        for i in self.colors:
            print(i)

class third(second):
    subject='python'
    def get_input(self):
        c=input('Please enter something')
        c.upper()  

# obj=first()                  parent can't access properties of child class
# print(obj.c1)
# obj.get_data('aman','python')

cobj=second()
print(cobj.colors)
print(cobj.c1)
cobj.get_data('Harry','Magic')
cobj.clrs()

obj2=third()
print(obj2.subject)
obj2.clrs()

        

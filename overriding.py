class parent:
    def hello:
        print('function inside parent class')

class child(parent):
    name='james'
    def hello(self):
        print('Child class function')      #if two classes have same functions then child class function runs this means overriding

ob=child()
ob.hello() 
print(ob.name)           
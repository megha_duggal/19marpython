class personal_details:
    type='student'
    def __init__(self,name,city,roll_no):
        self.name=name
        self.city=city
        self.roll_no=roll_no

    def print_info(self):
        print('Name:{}\nRoll_no:{}\nCity:{}'.format(self.name,self.roll_no,self.city)) 

class library_details:
    def books(self,name,issue_date,return_date):
        print('Book_name:{},ISD:{}\n,RD:{}'.format(name,issue_date,return_date))        

class fee_details(personal_details,library_details):
    def detail(self,pending,submitted):
        print('Pending Fee:Rs{}/-\nSubmitted:Rs{}/-'.format(pending,submitted))                

st1=personal_details('Aman',1,'Sunam')  
st1.print_info()
print(st1.type)  
st2=fee_details('Peter',2,'abc')
st2.print_info()
st2.detail(1000,2000)

student=fee_details('peter',2,'ahssdj')
student.print_info()
student.books('python programming','12-01-2019','02-02-2019')
student.detail(0,1000)
    